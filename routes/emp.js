const express=require('express')
const db=require('../db')
const utils=require('../utils')

const router=express.Router()

router.get('/',(request,response)=>{
    const connection=db.openConnection()
    const statement=`select * from emp`
    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))
    })
})
router.post('/',(request,response)=>{
    const {nam,salary,age}=request.body
    const connection=db.openConnection()
    const statement=`insert into emp values(default,'${nam}','${salary}','${age}')`
    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))
    })
})
router.put('/:id',(request,response)=>{
    const {id}=request.params
    const {nam,salary,age}=request.body
    const connection=db.openConnection()
    const statement=`update emp set name='${nam}',salary='${salary}',age='${age}' where id='${id}'`
    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))
    })
})

router.delete('/:id',(request,response)=>{
    const {id}=request.params
    const connection=db.openConnection()
    const statement=`delete from emp where id='${id}'`
    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))
    })
})

module.exports=router